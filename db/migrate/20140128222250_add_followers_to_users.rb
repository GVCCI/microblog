class AddFollowersToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :subscribers, :text
  end

  def self.down
    remove_column :users, :subscribers
  end
end
