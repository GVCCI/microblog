# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Microblog::Application.config.secret_key_base = 'de7e2b3d414551983fad490ace970e09132809e6ad04146790ce7774ddb164412367f91dab17f302b4e46b5527bc8cd410ead032b236d204961e44487b2b0ee6'
