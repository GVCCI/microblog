Microblog::Application.routes.draw do
  
  root 'main#index'

  devise_for :users, controllers: { registrations: "registrations" }

  devise_scope :user do
    get 'login' => 'devise/sessions#new'
    get 'join' => 'devise/registrations#new'
    get 'settings' => 'devise/registrations#edit'
  end

  resources :users do
  	member do
  		get :subscribed, :subscribers
  	end
  end
  resources :relationships
  resources :posts

end
