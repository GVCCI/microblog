class PostsController < ApplicationController

  before_filter :authenticate_user!, only: [:new, :create]

  def index
    @posts = Post.all
  end

  def show
    @post = Post.find(params[:id])
  end

  def new
    @post = Post.new
  end

  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      flash[:success] = "Post created"
      redirect_to root_url
    else
      flash[:error] = "Post cannot be created"
      redirect_to root_url
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    flash[:warning] = "Post deleted"
    redirect_to root_url
  end

	private

    def post_params
      params.require(:post).permit(:title, :content)
    end

end
