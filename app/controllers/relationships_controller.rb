class RelationshipsController < ApplicationController

  before_filter :authenticate_user!, only: [:create, :destroy]

  def create
    @user = User.find(params[:relationship][:subscribed_id])
    current_user.follow!(@user)
    redirect_to :back
  end

  def destroy
    @user = Relationship.find(params[:id]).subscribed
    current_user.unfollow!(@user)
    redirect_to :back
  end

end
