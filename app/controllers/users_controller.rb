class UsersController < ApplicationController

  def show
    @user = User.find(params[:id])
    @post = current_user.posts.build if signed_in?
    @posts = @user.posts
  end

  def subscribed
    @user = User.find(params[:id])
    @users = @user.subscribed_users
    render 'show_follow'
  end

  def subscribers
    @user = User.find(params[:id])
    @users = @user.subscribers
    render 'show_follow'
  end

end
