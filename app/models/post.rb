class Post < ActiveRecord::Base

  belongs_to :user
  default_scope -> { order('created_at DESC') }
  validates :title, presence: true
  validates :content, presence: true
  validates :content, length: { maximum: 140 }

  def self.from_users_subscribed_by(user)
    subscribed_user_ids = "SELECT subscribed_id FROM relationships
                         WHERE subscriber_id = :user_id"
    where("user_id IN (#{subscribed_user_ids}) OR user_id = :user_id",
          user_id: user.id)
  end

end
