class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  mount_uploader :avatar, AvatarUploader

  validates :username, uniqueness: { case_sensitive: false }

  validates_integrity_of  :avatar
  validates_processing_of :avatar

  has_many :posts
  has_many :relationships, foreign_key: "subscriber_id", dependent: :destroy
  has_many :subscribed_users, through: :relationships, source: :subscribed
  has_many :reverse_relationships, foreign_key: "subscribed_id", class_name: "Relationship", dependent: :destroy
  has_many :subscribers, through: :reverse_relationships, source: :subscriber

  def following?(other_user)
    relationships.find_by(subscribed_id: other_user.id)
  end

  def follow!(other_user)
    relationships.create!(subscribed_id: other_user.id)
  end

  def unfollow!(other_user)
    relationships.find_by(subscribed_id: other_user.id).destroy!
  end

  def feed
    Post.from_users_subscribed_by(self)
  end

end
